import React, {useState} from 'react';
import MemberState from "./context/memberState";
import './App.css';
import MemberList from "./components/MemberList";
import AddMember from "./components/AddMember";
import {Button} from "@material-ui/core";


function App() {
    const [toggleAdd, setToggleAdd] = useState(false);
    const toggleAddButton = () => {
        setToggleAdd(prevState => !prevState);
    }
  return (
      <MemberState>
          <div className='App-header'>
              {toggleAdd ? <AddMember cancel={()=>setToggleAdd(false)} ></AddMember>  : <Button onClick={toggleAddButton} variant="contained" color="primary">+ Add A New Member</Button>}
              <MemberList></MemberList>
          </div>
      </MemberState>
  );
}

export default App;
