import React, {useContext, useEffect, useState, Fragment} from 'react';
import MemberContext from "../context/memberContext";
import {Member} from "../types/member.d";
import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import ButtonGroup from '@material-ui/core/ButtonGroup';

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        borderRadius: 12,
        color: 'white'
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    select: {
        color: 'white',
        fontSize: '0.7em'
    },
    button: {
        backgroundColor: 'blue',
        borderRadius: 12,
        padding: 3
    }
}));

function MemberItem(props: {member: Member}) {
    const [_member, setMember] = useState(props.member);

    useEffect(()=> {
        setMember(props.member);
    }, [props.member])

    const classes = useStyles();

    const [editState, setEditState] = useState(false);
    const memberContext = useContext(MemberContext);
    const teamVals = Array.from(new Set(memberContext.members?.map((obj: Member) => obj.team)));
    const roleVals = Array.from(new Set(memberContext.members?.map((obj: Member) => obj.role)));



    const editRow = () => {
        setEditState(!editState);
        setMember(props.member);
        console.log(teamVals);
    }
    const editName = (event: any) => {
        const {value} = event.target;
        setMember({..._member, name: value})
    }

    const handleSelectChange = (event: React.ChangeEvent<{ value: unknown }>, type: string) => {
        setMember({..._member, [type]: event.target.value as string})
    };


    const submitChanges = () => {
        memberContext.updateMember?.(_member);
        setEditState(!editState);
    }

    const cancelChanges = () => {
        setMember(props.member);
        editRow()
    }


    return (
        <div className="member-item">
            { editState ? (
                <Fragment>
                    <input className="inputBox" name="name" value={_member.name} onChange={editName}/>
                    <FormControl className={classes.formControl}>
                        <InputLabel className={classes.select}>Team</InputLabel>
                        <Select
                            value={_member.team}
                            onChange={(e)=>handleSelectChange(e,'team')}
                            className={classes.select}
                        >
                            {teamVals && teamVals.map((option:string, index:number) => <MenuItem key={index} value={option}>{option}</MenuItem>)}
                        </Select>
                    </FormControl>
                    <FormControl className={classes.formControl}>
                        <InputLabel className={classes.select}>Role</InputLabel>
                        <Select
                            value={_member.role}
                            onChange={(e)=>handleSelectChange(e,'role')}
                            className={classes.select}
                        >
                            {roleVals && roleVals.map((option:string, index:number) => <MenuItem key={index} value={option}>{option}</MenuItem>)}
                        </Select>
                    </FormControl>
                    <div className='member-cell'>
                        <ButtonGroup variant="contained" color="primary" aria-label="contained primary button group">
                            <Button variant="contained" color="primary" onClick={submitChanges}>Save</Button>
                            <Button variant="contained"  color="secondary" onClick={cancelChanges}>Cancel</Button>
                        </ButtonGroup>
                    </div>

                </Fragment>
                ) :
                (<Fragment>
                <div className="member-cell">{_member.name}</div>
                <div className="member-cell">{_member.team}</div>
                <div className="member-cell">{_member.role}</div>
                <Button variant="contained" color="primary"  onClick={editRow}>Edit</Button>
            </Fragment>)
                }

        </div>

    );
}

export default MemberItem;
