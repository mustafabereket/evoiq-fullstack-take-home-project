import React, {useContext, useEffect, Fragment} from 'react';
import MemberContext from "../context/memberContext";
import MemberItem from "./MemberItem";
import {Member} from "../types/member.d";
import TableHeader from "./TableHeader";

function MemberList() {
    const memberContext = useContext(MemberContext);
    useEffect(()=> {memberContext.getMembers?.();},[])


    return (
        <Fragment>
                <TableHeader></TableHeader>
            <Fragment>
                {memberContext.members && memberContext.members.map((item: Member, index) => {
                    return (<MemberItem key={index} member={item}></MemberItem>)
                })}
            </Fragment>
        </Fragment>
    );
}

export default MemberList;
