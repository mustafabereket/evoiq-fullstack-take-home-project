import React, {useContext, useState} from 'react';
import MemberContext from "../context/memberContext";
import {Button} from "@material-ui/core";
import ButtonGroup from "@material-ui/core/ButtonGroup";

function AddMember(props: any) {
    const memberContext = useContext(MemberContext);
    const [member, updateMember] = useState({name: '', role: '', team: '', id: -1});
    const [errorState, updateErrorState] = useState(false);

    const handleChange = (event: React.ChangeEvent<{ value: string, name: string }>) => {
        updateErrorState(false);
        const {name, value} = event.target;
        updateMember({...member, [name]: value as string})
    };

    const save = () => {
        if(member.name && member.team && member.role){
            memberContext.saveMember?.(member);
            updateMember({name: '', role: '', team: '', id: -1});
            props.cancel();
        }else {
            updateErrorState(true);
        }
    }

    return (
        <div className='add-member'>
            <label >Name</label>
            <input className="inputBox" name="name" value={member.name} onChange={handleChange}/>

            <label >Team</label>
            <input className="inputBox" name="team" value={member.team} onChange={handleChange}/>

            <label >Role</label>
            <input className="inputBox" name="role" value={member.role} onChange={handleChange}/>
            <ButtonGroup variant="contained" color="primary" aria-label="contained primary button group">
                <Button variant="contained" color="primary" onClick={save}>Save</Button>
                <Button variant="contained" color="secondary" onClick={props.cancel}>Cancel</Button>
            </ButtonGroup>
            {errorState && <p className="error-msg">Please provide input for all of the entries.</p>}
        </div>
    );
}

export default AddMember;
