import React, {useContext} from 'react';
import MemberContext from "../context/memberContext";
import ArrowDownwardRoundedIcon from '@material-ui/icons/ArrowDownwardRounded';
import ArrowUpwardRoundedIcon from '@material-ui/icons/ArrowUpwardRounded';

function TableHeader() {

    const memberContext = useContext(MemberContext);

    const updateSortBy = (type: string) => {
        if(memberContext.updateSortBy){
            memberContext.updateSortBy(type, memberContext.sortBy?.order === 'asc' ? 'des' : 'asc');
        }
    }

    const retrieveArrows = () => {
        if(memberContext.sortBy?.order === 'asc') return (<ArrowDownwardRoundedIcon></ArrowDownwardRoundedIcon>);
        return (<ArrowUpwardRoundedIcon></ArrowUpwardRoundedIcon>)
    }

    return (
        <div className="table-header-row table-header">
            <div className="table-header-cell" onClick={()=>updateSortBy('name')}>Name {memberContext.sortBy?.type === 'name' && retrieveArrows()}</div>
            <div className="table-header-cell" onClick={()=>updateSortBy('team')}>Team {memberContext.sortBy?.type === 'team' && retrieveArrows()}</div>
            <div className="table-header-cell" onClick={()=>updateSortBy('role')}>Role {memberContext.sortBy?.type === 'role' && retrieveArrows()}</div>
            <div className="table-header-cell">Actions</div>
        </div>
    );
}

export default TableHeader;
