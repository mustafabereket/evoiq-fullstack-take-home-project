export default (state: any, action: { type: string; payload: any }) => {
    switch (action.type){
        case 'UPDATE_MEMBERS':
            return {
                ...state,
                members: action.payload
            }
        case 'UPDATE_SORT_BY':
            return {
                ...state,
                sortBy: action.payload.sortBy,
                members: action.payload.members
            }
        default:
            return {...state}

    }
}
