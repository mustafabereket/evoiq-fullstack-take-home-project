import MemberContext from "./memberContext";
import React, {useReducer} from "react";
import axios from "axios";
import memberReducer from "./memberReducer";
import {Member} from "../types/member.d";

const MemberState = (props: { children: React.ReactNode; }) => {
    const initialState = {members: [], sortBy: {type: 'name', order: 'asc'}}
    const [state, dispatch] = useReducer(memberReducer, initialState);

    const getMembers = async () => {
        const {data} = await axios.get('http://localhost:8080/api/members');
        return dispatch({
            type: 'UPDATE_MEMBERS',
            payload: data.members
        });
    }

    const saveMember = async (member: Member) => {
        const {data} = await axios.post('http://localhost:8080/api/addMember', member);
        dispatch({
            type: 'UPDATE_MEMBERS',
            payload: data.members
        });
    }

    const updateSortBy = (type: string, order: string ) => {
        const data = state.members.sort((a: any, b: any) => {
            if(order === 'asc'){ return a[type].localeCompare(b[type])}
            if(order === 'des'){ return b[type].localeCompare(a[type])}
        })

        dispatch({
            type: 'UPDATE_SORT_BY',
            payload: {
                sortBy: {type, order},
                members: [...data]
            }
        })
    }

    const updateMember = async (member: Member) => {
        const {data} = await axios.patch('http://localhost:8080/api/updateMember', member);
        return dispatch({
            type: 'UPDATE_MEMBERS',
            payload: data.members
        });
    }

    return (
        <MemberContext.Provider value={{
            members: state.members,
            sortBy: state.sortBy,
            getMembers,
            updateSortBy,
            saveMember,
            updateMember,
        }}>
            {props.children}
        </MemberContext.Provider>
    )
}

export default MemberState;
