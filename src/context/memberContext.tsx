import React from "react";
import {Member} from "../types/member.d";

type ContextProps = {
    members: Member[];
    getMembers(): Promise<void>;
    sortBy: {
        type: string;
        order: string;
    };
    updateSortBy(type: string, order: string ): void;
    saveMember(member: Member): Promise<void>;
    updateMember(member: Member): Promise<void>;
}

const MemberContext = React.createContext<Partial<ContextProps>>({});

export default MemberContext;
