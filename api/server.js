const express = require('express');
const path = require('path');
const sqlite3 = require('sqlite3').verbose();
const cors = require('cors');

const app = express();
const getQuery = 'SELECT id, name, team, role FROM Members'
// Global sqlite db connection
let db = new sqlite3.Database('./api/db/sample.db', sqlite3.OPEN_READWRITE, (err) => {
  if (err) {
    throw err;
  }
  console.log('Connected to the sqlite database.');
});
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());

// Health test
app.get('/api/health', function (req, res) {
 return res.send('OK');
});

// Get all team members from Members table
app.get('/api/members', function (req, res) {
  console.log('girdi ');
  db.all(getQuery, [], (err, rows) => {
    if (err) {
      throw err;
    }
    return res.json({members: rows});
  });
});

app.patch('/api/updateMember', function (req, res) {
  let sql = `UPDATE Members SET name="${req.body.name}", team="${req.body.team}", role="${req.body.role}" WHERE id="${req.body.id}"`;
  console.log('UPDATE ROW');
  db.run(sql, [], (err, rows) => {
    if (err) {
      throw err;
    }

    db.all(getQuery, [], (err, rows) => {
      if (err) {
        throw err;
      }
      return res.json({members: rows});
    });

  });
});


app.post('/api/addMember', function (req, res) {
  let sql = `INSERT INTO Members (name, team, role) VALUES ("${req.body.name}", "${req.body.team}", "${req.body.role}")`;
  db.run(sql, [], (err, rows) => {
    if (err) {
      throw err;
    }

    db.all(getQuery, [], (err, rows) => {
      if (err) {
        throw err;
      }
      return res.json({members: rows});
    });

  });
});

app.listen(8080);
